/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package senhordosmaneis;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jbeir
 */
public class MapaTest {

    public MapaTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of criaMapa method, of class Mapa.
     */
    @Test
    public void testCriaMapa() {
        System.out.println("criaMapa");
        List jogTeste = new ArrayList<Personagem>();
        jogTeste.add(new Personagem(0, "teste", 0, 0));
        Mapa teste = new Mapa(jogTeste);
        teste.criaMapa();
        assertEquals(teste, teste);
    }

    /**
     * Test of getPersonagem method, of class Mapa.
     */
    @Test
    public void testGetPersonagem() {
        System.out.println("getPersonagem");
        char c = '0';
        List jogTeste = new ArrayList<Personagem>();
        Personagem p = new Personagem(0, "teste", 0, 0);
        jogTeste.add(p);
        Mapa mapaTeste = new Mapa(jogTeste);
        Personagem expResult = mapaTeste.getPersonagem(c);
        Personagem result = mapaTeste.getPersonagem(c);
        assertEquals(expResult, result);
    }

    /**
     * Test of jogaTurno method, of class Mapa.
     */
    @Test
    public void testJogaTurno() {
        System.out.println("jogaTurno");
        List<Personagem> jogTeste = new ArrayList<Personagem>();
        Personagem p1 = new Personagem(0, "teste1", 0, 0);
        Personagem p2 = new Personagem(1, "teste2", 0, 0);
        jogTeste.add(p1);
        jogTeste.add(p2);
        Mapa mapaTeste = new Mapa(jogTeste);
        mapaTeste.jogaTurno();
    }

    /**
     * Test of moveJogador method, of class Mapa.
     */
    @Test
    public void testMoveJogador() {
        System.out.println("moveJogador");
        List<Personagem> jogTeste = new ArrayList<Personagem>();
        Personagem p1 = new Personagem(0, "teste1", 0, 0);
        Personagem p2 = new Personagem(1, "teste2", 0, 0);
        jogTeste.add(p1);
        jogTeste.add(p2);
        Mapa mapaTeste = new Mapa(jogTeste);
        // Personagem 1 - esta na posicao (0, 3) com Orientacao para Norte
        // Personagem 2 - esta na posicao (1, 0) com Orientacao para Norte
        boolean expResult1 = false; // p1 nao se pode mover para norte, logo false
        boolean expResult2 = true; // p2 pode mover-se para norte, logo true
        boolean resultFalse = mapaTeste.moveJogador(p1);
        boolean resultTrue = mapaTeste.moveJogador(p2);
        assertEquals(expResult1, resultFalse);
        assertEquals(expResult2, resultTrue);
    }

    /**
     * Test of getPosXY method, of class Mapa.
     */
    @Test
    public void testGetPosXY() {
        System.out.println("getPosXY");
        List jogTeste = new ArrayList<Personagem>();
        Personagem p1 = new Personagem(0, "teste1", 0, 0);
        Personagem p2 = new Personagem(0, "teste2", 0, 0);
        jogTeste.add(p1);
        jogTeste.add(p2);
        Mapa mapaTeste = new Mapa(jogTeste);
        int[] expResult = mapaTeste.getPosXY(p2);
        int[] result = mapaTeste.getPosXY(p1);
        assertArrayEquals(expResult, result);
    }

}
