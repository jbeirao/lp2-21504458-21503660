package senhordosmaneis;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SenhorDosManeis {

    public static void main(String[] args) {
        List<Personagem> listaJogadoresOriginal = criaListaJogadores();
        List<Personagem> listaJogadores = criaListaJogadores();
        Mapa mapaOriginal = new Mapa(listaJogadoresOriginal);
        Mapa mapaJogo = new Mapa(listaJogadores);
        mapaJogo.jogaTurno();
        criaRelatorio(mapaJogo, mapaOriginal, listaJogadores, listaJogadoresOriginal);
    }
    
    public static List<Personagem> criaListaJogadores() {
        List jogadores = new ArrayList<Personagem>();
        try {
            Scanner leitor = new Scanner(new FileReader("personagens.txt"));
            while(leitor.hasNext()) {
                String linhaInput = leitor.next();
                jogadores.add(criaPersonagem(linhaInput));
            }
            leitor.close();
        } catch(FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
        return jogadores;
    }
    
    public static Personagem criaPersonagem(String linha) {
        String[] dados = linha.split("\\:");
        int id = Integer.parseInt(dados[0]);
        String nome = dados[1];
        int tipo = Integer.parseInt(dados[2]);
        int orientacao = Integer.parseInt(dados[3]);
        Personagem jogador = new Personagem(id, nome, tipo, orientacao);
        return jogador;
    }
    
    public static void criaRelatorio(Mapa mapaJogo, Mapa mapaOriginal,
            List<Personagem> jogadores, List<Personagem> jogadoresOriginal) {
        try {
            File relatorio = new File("relatorio.txt");
            int personagemActual = 0;
            BufferedWriter bf = new BufferedWriter(new FileWriter(relatorio));
            bf.write("Estado Inicial");
            bf.newLine();
            // ***************** Escreve mapa Original *****************
            for (List<Personagem> linha : mapaOriginal.mapa) {
                for (Personagem p : linha) {
                     if(p.id == -1)  bf.write("V");
                     else bf.write(String.valueOf(p.id));
                }
                bf.newLine();
            }
            bf.newLine();
            //**********************************************************
            // ***************** Escreve mapa Final *****************
            bf.write("Estado Final");
            bf.newLine();
            for (List<Personagem> linha : mapaJogo.mapa) {
                for (Personagem p : linha) {
                     if(p.id == -1)  bf.write("V");
                     else bf.write(String.valueOf(p.id));
                }
                bf.newLine();
            }
            bf.newLine();
            //**********************************************************
            // ***************** Escreve Personagens *****************
            bf.write("Personagens");
            bf.newLine();
            for(Personagem p : jogadores) {
                bf.write(p.nome + " (" + p.getTipo() +")");
                bf.newLine();
                int[] posIni = mapaOriginal.getPosXY(p);
                bf.write(String.valueOf(posIni[0]) + "," +  String.valueOf(posIni[1]));
                String orientacao = jogadoresOriginal.get(personagemActual).getOrientacao();
                bf.write(" (" + orientacao + ")");
                bf.newLine();
                int[] posFin = mapaJogo.getPosXY(p);
                bf.write(String.valueOf(posFin[0]) + "," +  String.valueOf(posFin[1]));
                bf.write(" (" + p.getOrientacao() + ")");
                bf.newLine();
                bf.newLine();
                personagemActual++;
            }
            bf.close();
            
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
    
}