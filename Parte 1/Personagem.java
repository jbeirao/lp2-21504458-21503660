package senhordosmaneis;

public class Personagem {
    String nome;
    int orientacao;
    int tipo;
    int id;

    public Personagem () {
        this.nome = "V";
        this.orientacao = 0;
        this.tipo = 0;
        this.id = -1;
    }
    
    public Personagem (int id, String nome, int tipo, int orientacao) {
        this.nome = nome;
        this.id = id;
        this.tipo = tipo;
        this.orientacao = orientacao;
    }
    
    public void mudaOrientacao() {
        if(this.orientacao == 6) this.orientacao = 0;
        else this.orientacao += 2;
    }
    
    public String getOrientacao() {
        String orientacao = "";
        switch(this.orientacao)
        {
            case 0 : orientacao = "Norte";break;
            case 2 : orientacao = "Este";break;
            case 4 : orientacao = "Sul";break;
            case 6 : orientacao = "Oeste";break;
        }
        return orientacao;
    }
        
    public String getTipo() {
        String tipo = "";
        switch(this.tipo)
        {
            case 0 : tipo = "Anão";break;
            case 1 : tipo = "Dragão";break;
            case 2 : tipo = "Elfo";break;
            case 3 : tipo = "Gigante";break;
            case 4 : tipo = "Humano";break;
        }
        return tipo;
    }
}
