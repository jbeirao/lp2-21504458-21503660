package senhordosmaneis;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Mapa {
    List<List<Personagem>> mapa;
    List<Personagem> jogadores;
    
    public Mapa(List<Personagem> jogadores) {
        this.jogadores = jogadores;
        this.mapa = new ArrayList<List<Personagem>>();
        
        //**************************************************************
        // Le do ficheiro e cria Listas de Listas mapaOriginal e mapaJogo
        //**************************************************************
        criaMapa();
    }
    
    public void criaMapa() {
        try {
            Scanner input = new Scanner(new FileReader("mapa.txt"));
            int linha = 0;
            while(input.hasNext()) {
                char[] linhaInput = input.next().toCharArray();
                this.mapa.add(new ArrayList<Personagem>());
                for (char c : linhaInput) {
                    this.mapa.get(linha).add(getPersonagem(c));
                }
                linha++;
            }
            input.close();
        } catch(FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public Personagem getPersonagem (char c) {
        if (c != 'V') {
            int i = 0, id = Character.getNumericValue(c);
            for (Personagem p : this.jogadores) {
                if (p.id == id) return this.jogadores.get(i);
                i++;
            }
        }
        return new Personagem();
    }
    
    public void jogaTurno () {
        int linha = 0;
        for (Personagem jogador : this.jogadores) {
            boolean moveJogador = moveJogador(jogador);
            if(!moveJogador) {
                jogador.mudaOrientacao();
            }
            linha++;
        }
    }
    
    public boolean moveJogador(Personagem p) {
        int[] pos = getPosXY(p);
        Personagem pvazio = new Personagem();
        switch(p.orientacao)
        {
            case 0: 
                if (pos[0] - 1 >= 0) {
                    if (this.mapa.get(pos[0]-1).get(pos[1]).nome.equals(pvazio.nome)) {
                        this.mapa.get(pos[0]-1).set(pos[1], p);
                        this.mapa.get(pos[0]).set(pos[1], pvazio);
                    }
                }
                else {
                    return false;
                }
                break;
            case 2: 
                if (pos[1] + 1 < this.mapa.get(pos[0]).size()) {
                    if (this.mapa.get(pos[0]).get(pos[1] + 1).nome.equals(pvazio.nome)) {
                        this.mapa.get(pos[0]).set(pos[1] + 1, p);
                        this.mapa.get(pos[0]).set(pos[1], pvazio);
                    }
                }
                else {
                    return false;
                }
                break;
            case 4: 
                if (pos[0] + 1 < this.mapa.size()) {
                    if (this.mapa.get(pos[0]+1).get(pos[1]).nome.equals(pvazio.nome)) {
                        this.mapa.get(pos[0]+1).set(pos[1], p);
                        this.mapa.get(pos[0]).set(pos[1], pvazio);
                    }
                }
                else {
                    return false;
                }
                break;
            case 6: 
                if (pos[1] - 1 >= 0) {
                    if (this.mapa.get(pos[0]).get(pos[1] - 1).nome.equals(pvazio.nome)) {
                        this.mapa.get(pos[0]).set(pos[1] - 1, p);
                        this.mapa.get(pos[0]).set(pos[1], pvazio);                    
                    }
                }
                else {
                    return false;
                }
                break;
            }
            return true;
        }
    
    public int[] getPosXY(Personagem p) {
        int x, y;
        int[] pos = {0, 0};
        for (x = 0; x < this.mapa.size(); x++) {
            for (y = 0; y < this.mapa.get(x).size(); y++) {
                if (this.mapa.get(x).get(y).id == p.id) {
                    pos[0] = x;
                    pos[1] = y;
                    break;
                }
            }
        }
        return pos;
    }
}
