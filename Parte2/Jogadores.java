package senhordosmaneis2;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Jogadores {
    
    private List<Personagem> listaJogadores;
    
    public Jogadores() {
         try {
            listaJogadores = new ArrayList<Personagem>();
            Scanner input = new Scanner(new FileReader("personagens.txt"));
            while(input.hasNext()) {
                String[] linhaInput = input.next().split("\\:");
                String nome = linhaInput[1];
                int orientacao = Integer.parseInt(linhaInput[3]);
                int tipo = Integer.parseInt(linhaInput[2]);
                int id = Integer.parseInt(linhaInput[0]); ;
               switch(tipo) {
                    case 0: listaJogadores.add(new Anao(id, nome, tipo, orientacao));break;
                    case 1: listaJogadores.add(new Dragao(id, nome, tipo, orientacao));break;
                    case 2: listaJogadores.add(new Elfo(id, nome, tipo, orientacao));break;
                    case 3: listaJogadores.add(new Gigante(id, nome, tipo, orientacao));break;
                    case 4: listaJogadores.add(new Humano(id, nome, tipo, orientacao));break;
                }
            }
            input.close();
        } catch(FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void imprime() {
        for (Personagem p : this.listaJogadores) {
            System.out.println(p.getNome());
        }
    }
    
    public List<Personagem> getJogadores() {
        return this.listaJogadores;
    }
    
    public Personagem getChar(char c) {
        int id = Character.getNumericValue(c);
        return this.listaJogadores.get(id);
    }
    
    public void movePersonagem(List<List<Personagem>> mapa) {
    }
}
