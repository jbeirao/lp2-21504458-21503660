package senhordosmaneis2;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Mapa {
    public List<List<Personagem>> mapa;
    Jogadores jogadores;
    
    public Mapa(Jogadores jogadores) {
        this.jogadores = jogadores;
        this.mapa = new ArrayList<List<Personagem>>();
        //**************************************************************
        // Le do ficheiro e cria Listas de Listas mapaOriginal e mapaJogo
        //**************************************************************
        criaMapa();
    }
    
    public void criaMapa() {
        try {
            Scanner input = new Scanner(new FileReader("mapa.txt"));
            int x = 0, y;
            while(input.hasNext()) {
                char[] linhaInput = input.next().toCharArray();
                this.mapa.add(new ArrayList<Personagem>());
                y = 0;
                for (char c : linhaInput) {
                    Personagem temp = getPersonagem(c);
                    temp.setPos(x, y);
                    this.mapa.get(x).add(temp);
                    y++;
                }
                x++;
            }
            input.close();
        } catch(FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void jogaTurno() {
        for (Personagem jogador : this.jogadores.getJogadores()) {
            jogador.movePersonagem(this.mapa);
        }
    }
    
    public Personagem getPersonagem (char c) {
        Personagem p;
        switch(c) {
            case 'V': p = new Vazio(); break;
            case 'Z': p = new Buraco();break;
            case 'O': p = new Tesouro("O");break;
            case 'P': p = new Tesouro("P");break;
            case 'B': p = new Tesouro("B");break;
            default:
                p = jogadores.getChar(c);
                break;
        }
        return p;
    }
    
    public List<List<Personagem>> getMapa() {
        return this.mapa;
    }
    
    public void mostra() {
        System.out.println("-------------------------------------------");
        for (List<Personagem> l : this.mapa) {
            for (Personagem p : l) {
                if (p.getNome().length() > 1) System.out.print(p.getID());
                else System.out.print(p.getNome());
            }
            System.out.println("");
        }
        System.out.println("-------------------------------------------");
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (List<Personagem> l : this.mapa) {
            for (Personagem p : l) {
                if (p.getNome().length() > 1) sb.append(p.getID());
                else sb.append(p.getNome());
            }
            sb.append(System.getProperty("line.separator"));
        }
        return sb.toString();
    }
}