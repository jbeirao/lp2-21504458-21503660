package senhordosmaneis2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Personagem {
    protected int orientacao;
    protected int tipo;
    protected int id;
    protected int x;
    protected int y;
    protected String nome;
    protected boolean isChar = false;
    
    
    public boolean isCharacter() {
        return this.isChar;
    }
    public int getX() {
        return this.x;
    }
    
    public int getY() {
        return this.y;
    }
    
    public String getPos() {
        return "(" + getX() + "," + getY() + ")";
    }
    
    public void setPos(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    public String getNome() {
        return this.nome;
    }
    
    public int getID() {
        return this.id;
    }

    void movePersonagem(List<List<Personagem>> mapa) {
    }
    
    public List<String> getTesouros() {
        return new ArrayList<String>();
    }
    
}