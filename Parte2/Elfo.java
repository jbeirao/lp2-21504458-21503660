package senhordosmaneis2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Elfo extends Personagem{
    private int[] tesouros = { 0, 0, 0, 0};
    private final int passos = 2;
    
    public Elfo (int id, String nome, int tipo, int orientacao) {
        this.nome = nome;
        this.id = id;
        this.tipo = tipo;
        this.orientacao = orientacao;
        this.isChar = true;
    }
    
    public void mudaOrientacao() {
        if(this.orientacao == 7) this.orientacao = 0;
        else this.orientacao ++;
    }
    
    public String getOrientacao() {
        String orientacao = "";
        switch(this.orientacao)
        {
            case 0 : orientacao = "Norte";break;
            case 1 : orientacao = "Nordeste";break;
            case 2 : orientacao = "Este";break;
            case 3 : orientacao = "Sudeste";break;
            case 4 : orientacao = "Sul";break;
            case 5 : orientacao = "Sudoeste";break;
            case 6 : orientacao = "Oeste";break;
            case 7 : orientacao = "Nordeste";break;
        }
        return orientacao;
    }
    
    public void movePersonagem(List<List<Personagem>> mapa) {
        int x = getX();
        int y = getY();
        Personagem vazio = new Vazio();
        boolean valida = false;
        System.out.println("orintacao: " + this.orientacao);
        switch (this.orientacao) {
            case 0: 
                if ( (x-passos) >= 0) {
                    if (!mapa.get(x-passos).get(y).getNome().equals("Z") && !mapa.get(x-passos).get(y).isCharacter()) {
                        if (!mapa.get(x-1).get(y).isCharacter()) {
                            switch (mapa.get(x-passos).get(y).getNome()) {
                                case "V":break;
                                case "B": this.tesouros[0] += 1;
                                          this.tesouros[3]++;
                                          break;
                                case "P": this.tesouros[0] += 2;
                                          this.tesouros[2]++;
                                          break;
                                case "O": this.tesouros[0] += 3;
                                          this.tesouros[1]++;
                                          break;
                            }
                            mapa.get(x-passos).set(y, mapa.get(x).get(y));
                            mapa.get(x).set(y, vazio);
                            mapa.get(x-passos).get(y).setPos(x-2, y);
                            mapa.get(x).get(y).setPos(x, y);
                            valida = true;
                        }
                    }
                }
                break;
            case 1: 
                if ( (x-passos) >= 0 && (y+passos) < mapa.get(x).size()) {
                    if (!mapa.get(x-passos).get(y+passos).getNome().equals("Z") && !mapa.get(x-passos).get(y+passos).isCharacter()) {
                        if (!mapa.get(x-1).get(y+1).isCharacter()) {
                            switch (mapa.get(x-passos).get(y+2).getNome()) {
                                case "V":break;
                                case "B": this.tesouros[0] += 1;
                                          this.tesouros[3]++;
                                          break;
                                case "P": this.tesouros[0] += 2;
                                          this.tesouros[2]++;
                                          break;
                                case "O": this.tesouros[0] += 3;
                                          this.tesouros[1]++;
                                          break;
                            }
                            mapa.get(x-passos).set(y+passos, mapa.get(x).get(y));
                            mapa.get(x).set(y, vazio);
                            mapa.get(x-passos).get(y+passos).setPos(x-2, y+2);
                            mapa.get(x).get(y).setPos(x, y);

                            valida = true;
                        }
                    }
                }
                break;
            case 2: 
                if ((y+passos) < mapa.get(x).size()) {
                    if (!mapa.get(x).get(y+passos).getNome().equals("Z") && !mapa.get(x).get(y+passos).isCharacter()) {
                        if (!mapa.get(x).get(y+1).isCharacter()) {
                            switch (mapa.get(x).get(y+passos).getNome()) {
                                case "V":break;
                                case "B": this.tesouros[0] += 1;
                                          this.tesouros[3]++;
                                          break;
                                case "P": this.tesouros[0] += 2;
                                          this.tesouros[2]++;
                                          break;
                                case "O": this.tesouros[0] += 3;
                                          this.tesouros[1]++;
                                          break;
                            }
                            mapa.get(x).set(y+passos, mapa.get(x).get(y));
                            mapa.get(x).set(y, vazio);
                            mapa.get(x).get(y+passos).setPos(x, y+2);
                            mapa.get(x).get(y).setPos(x, y);

                            valida = true;
                        }
                    }
                }
                break;
            case 3: 
                if ( (x+passos) < mapa.size() && (y+passos) < mapa.get(x).size()) {
                    if (!mapa.get(x+passos).get(y+passos).getNome().equalsIgnoreCase("Z") && !mapa.get(x+passos).get(y+passos).isCharacter()) {
                        if (!mapa.get(x+1).get(y+1).isCharacter()) {
                            switch (mapa.get(x+passos).get(y+2).getNome()) {
                                case "V":break;
                                case "B": this.tesouros[0] += 1;
                                          this.tesouros[3]++;
                                          break;
                                case "P": this.tesouros[0] += 2;
                                          this.tesouros[2]++;
                                          break;
                                case "O": this.tesouros[0] += 3;
                                          this.tesouros[1]++;
                                          break;
                            }
                            mapa.get(x+passos).set(y+passos, mapa.get(x).get(y));
                            mapa.get(x).set(y, vazio);
                            mapa.get(x+passos).get(y+passos).setPos(x+2, y+2);
                            mapa.get(x).get(y).setPos(x, y);

                            valida = true;
                        }
                    }
                }
                break;
            case 4: 
                if ( (x+passos) < mapa.size()) {
                    if (!mapa.get(x+passos).get(y).getNome().equals("Z") && !mapa.get(x+passos).get(y).isCharacter()) {
                        if (!mapa.get(x+1).get(y).isCharacter()) {
                            switch (mapa.get(x+passos).get(y).getNome()) {
                                case "V":break;
                                case "B": this.tesouros[0] += 1;
                                          this.tesouros[3]++;
                                          break;
                                case "P": this.tesouros[0] += 2;
                                          this.tesouros[2]++;
                                          break;
                                case "O": this.tesouros[0] += 3;
                                          this.tesouros[1]++;
                                          break;
                            }
                            mapa.get(x+passos).set(y, mapa.get(x).get(y));
                            mapa.get(x).set(y, vazio);
                            mapa.get(x+passos).get(y).setPos(x+2, y);
                            mapa.get(x).get(y).setPos(x, y);
                        }
                    }
                }
                break;
            case 5: 
                if ( (x+passos) < mapa.size() && (y-passos) >= 0) {
                    if (!mapa.get(x+passos).get(y-passos).getNome().equals("Z") && !mapa.get(x+passos).get(y-passos).isCharacter()) {
                        if (!mapa.get(x+1).get(y-1).isCharacter()) {
                            switch (mapa.get(x+passos).get(y-passos).getNome()) {
                                case "V":break;
                                case "B": this.tesouros[0] += 1;
                                          this.tesouros[3]++;
                                          break;
                                case "P": this.tesouros[0] += 2;
                                          this.tesouros[2]++;
                                          break;
                                case "O": this.tesouros[0] += 3;
                                          this.tesouros[1]++;
                                          break;
                            }
                            mapa.get(x+passos).set(y-passos, mapa.get(x).get(y));
                            mapa.get(x).set(y, vazio);
                            mapa.get(x+passos).get(y-passos).setPos(x+2, y-2);
                            mapa.get(x).get(y).setPos(x, y);
                            valida = true;
                        }
                    }
                }
                break;
            case 6: 
                if ((y-passos) >= 0) {
                    if (!mapa.get(x).get(y-passos).getNome().equals("Z") && !mapa.get(x).get(y-passos).isCharacter()) {
                        if (!mapa.get(x).get(y-1).isCharacter()) {
                            switch (mapa.get(x).get(y-passos).getNome()) {
                                case "V":break;
                                case "B": this.tesouros[0] += 1;
                                          this.tesouros[3]++;
                                          break;
                                case "P": this.tesouros[0] += 2;
                                          this.tesouros[2]++;
                                          break;
                                case "O": this.tesouros[0] += 3;
                                          this.tesouros[1]++;
                                          break;
                            }
                            mapa.get(x).set(y-passos, mapa.get(x).get(y));
                            mapa.get(x).set(y, vazio);
                            mapa.get(x).get(y-passos).setPos(x, y-2);
                            mapa.get(x).get(y).setPos(x, y);

                            valida = true;
                        }
                    }
                }
                break;
            case 7: 
                if ( (x-passos) >= 0 && (y-passos) >= 0) {
                    if (!mapa.get(x-passos).get(y-passos).getNome().equals("Z") && !mapa.get(x-passos).get(y-passos).isCharacter()) {
                        if (!mapa.get(x-1).get(y-1).isCharacter()) {
                            switch (mapa.get(x-passos).get(y-2).getNome()) {
                                case "V":break;
                                case "B": this.tesouros[0] += 1;
                                          this.tesouros[3]++;
                                          break;
                                case "P": this.tesouros[0] += 2;
                                          this.tesouros[2]++;
                                          break;
                                case "O": this.tesouros[0] += 3;
                                          this.tesouros[1]++;
                                          break;
                            }
                            mapa.get(x-passos).set(y-passos, mapa.get(x).get(y));
                            mapa.get(x).set(y, vazio);
                            mapa.get(x-passos).get(y-passos).setPos(x-2, y-2);
                            mapa.get(x).get(y).setPos(x, y);

                            valida = true;
                        }
                    }
                }
                break;
        }
        if (!valida) mudaOrientacao();
    }
    
    @Override
    public List<String> getTesouros() {
        List<String> tesouros = new ArrayList<String>();
        tesouros.add(this.nome);
        for(int i = 0; i < this.tesouros.length; i++) {
            tesouros.add(Integer.toString(this.tesouros[i]));
        }
        return tesouros;
    }
}
