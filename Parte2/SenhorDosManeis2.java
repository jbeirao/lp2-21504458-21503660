package senhordosmaneis2;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SenhorDosManeis2 {

    public static void main(String[] args) {
        Jogadores jogadores = new Jogadores();
        Mapa mapaOriginal = new Mapa(jogadores);
        Mapa mapaJogo = new Mapa(jogadores);
        
        try{
            PrintWriter writer = new PrintWriter("relatorio.txt");
            writer.println("Estado Inicial");
            writer.println(mapaJogo.toString());
            writer.println("Turno 1");
            mapaJogo.jogaTurno();
            writer.println(mapaJogo.toString());
            writer.println("Turno 2");
            mapaJogo.jogaTurno();
            writer.println(mapaJogo.toString());
            writer.println("Turno 3");
            mapaJogo.jogaTurno();
            writer.println(mapaJogo.toString());
            writer.println("Turno 4");
            mapaJogo.jogaTurno();
            writer.println(mapaJogo.toString());
            writer.println("Turno 5");
            mapaJogo.jogaTurno();
            writer.println(mapaJogo.toString());
            writer.println();
            writer.println("Tesouros");
            List<ArrayList<String>> premios = new ArrayList<ArrayList<String>>();
            for (Personagem p : jogadores.getJogadores()) {
                premios.add(new ArrayList<String>(p.getTesouros()));
            }
            Collections.sort(premios, new Comparator<ArrayList<String>>() {    
                    @Override
                    public int compare(ArrayList<String> o1, ArrayList<String> o2) {
                        return o2.get(1).compareTo(o1.get(1));
                    }               
            });
            
            for (ArrayList<String> l : premios) {
                for (String dado : l) {
                     writer.print(dado + " ");
                }
                writer.println();
            }
            
            
            writer.println();

            writer.close();
        } catch (IOException e) {
           System.out.println(e.getMessage());
        }
    }
    
}